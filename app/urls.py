from django.urls import path
from . import views

app_name='app'

urlpatterns=[
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('works/', views.works, name='works '),
    path('message/', views.message, name='message'),
    path('contact/', views.contact, name="contact"),
    path('msgcheck/', views.msgcheck, name="msgcheck"),
]