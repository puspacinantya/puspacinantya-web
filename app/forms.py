from django import forms
from .models import Messagemodels

class Messageform(forms.Form):
    name = forms.CharField(label='Name', required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text', 'placeholder': 'required'}))
    email = forms.EmailField(label='Email', required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'required'}))
    message = forms.CharField(label='Message', required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text', 'placeholder': 'required'}))