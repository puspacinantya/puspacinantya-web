from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home

# Create your tests here.

class WebTest(TestCase):
    def test_using_home(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_works(self):
        response = Client().get('/works/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_template(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

