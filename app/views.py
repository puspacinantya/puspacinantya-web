from django.shortcuts import render
from django.http import JsonResponse
from django.db import IntegrityError
from .forms import Messageform
from .models import Messagemodels

# Create your views here.
response={}

def home(request):
    return render(request, 'home.html', response)

def about(request):
    return render(request, 'about.html', response)

def works(request):
    return render(request, 'works.html', response)

def msgcheck(request):
    return render(request, 'msgcheck.html', response)

def message(request):
    response['savemessage'] = Messageform
    table = Messagemodels.objects.all()
    response['table'] = table
    html = 'message.html'

    if(request.method == 'POST'):
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['message'] = request.POST['message']
        try:
            writestat = Messagemodels(name=response['name'], email=response['email'], message=response['message'])
            writestat.save()
            status = 'valid'
            return JsonResponse({'stat':status})
        except IntegrityError as e:
            status = 'invalid'
            return JsonResponse({'stat':status})
    else:
        return render(request, 'message.html', response)

def contact(request):
    return render(request, 'contact.html', response)