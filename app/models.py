from django.db import models
from django.utils import timezone

# Create your models here.

class Messagemodels(models.Model):
    name = models.TextField(max_length=35)
    email = models.EmailField(max_length=254)
    message = models.TextField(max_length=140)
    date = models.DateField(default=timezone.now)
