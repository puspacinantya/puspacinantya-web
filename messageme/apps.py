from django.apps import AppConfig


class MessagemeConfig(AppConfig):
    name = 'messageme'
