# **Puspacinantya's personal web**

This project is made on Puspa's winter holiday as a something to fill in the free time. 

## Status pipelines
![Pipeline status](https://gitlab.com/puspacinantya/puspacinantya-web/badges/master/build.svg)

## Test Coverage
![Django Coverage](https://gitlab.com/puspacinantya/puspacinantya-web/badges/master/coverage.svg)

## Coverage Report
[![pipeline status](https://gitlab.com/puspacinantya/puspacinantya-web/badges/master/pipeline.svg)](https://gitlab.com/puspacinantya/puspacinantya-web/commits/master)

## Link Herokuapp
https://puspacinantya.herokuapp.com/

